﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    GameObject targetEnemy;
    GameObject[] enemies;
    public int distance;
    EnemyTec healthEn;
    [SerializeField] private GameObject explosion;

	// Use this for initialization
	void Start ()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
	}
	
	// Update is called once per frame
	void Update ()
    {
        //LookAt
        enemies = GameObject.FindGameObjectsWithTag("Enemy");

        for (int i = 0; i < enemies.Length; i++)
        {
            if(Vector3.Distance(enemies[i].transform.position, this.transform.position) < distance)
            {
                if(targetEnemy == null)
                {
                    targetEnemy = enemies[i];
                    StartCoroutine(Shoot());
                }
                this.transform.LookAt(targetEnemy.transform);

                if (Vector3.Distance(targetEnemy.transform.position, this.transform.position) >= distance)
                {
                    targetEnemy = null;
                    StopAllCoroutines();
                }

            }
        }
    }

    public IEnumerator Shoot()
    {
        while (targetEnemy != null)
        {
            healthEn = targetEnemy.GetComponent<EnemyTec>();
            healthEn.enemyHealth -= 1;
            Instantiate(explosion, healthEn.transform.position, Quaternion.identity);
            yield return new WaitForSeconds(1.0f);
        }
    }
}
