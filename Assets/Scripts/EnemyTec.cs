﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyTec : MonoBehaviour {

    NavMeshAgent agent;
    GameObject target;
    string healthUI;
    public int enemyHealth;
    string energyUI;
    [SerializeField] private GameObject explosion;

	// Use this for initialization
	void Start ()
    {
        agent = GetComponent<NavMeshAgent>();
        target = GameObject.Find("PlayerBaseTarget");
        agent.SetDestination(target.transform.position);


        enemyHealth = 10;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (enemyHealth < 1)
        {
            energyUI = GameObject.Find("MoneyText").GetComponent<Text>().text;
            float energyF = float.Parse(energyUI);
            energyF += 4;
            string newEnergy = energyF.ToString();
            GameObject.Find("MoneyText").GetComponent<Text>().text = newEnergy;
            Destroy(gameObject);

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Vector3 headQuarters = GameObject.Find("HeadQuarters").transform.position;


        if (other.gameObject.tag == "finale")
        {
            healthUI = GameObject.Find("HealthText").GetComponent<Text>().text;
            float healthF = float.Parse(healthUI);
            healthF -= 1;

            if(healthF <= 0)
            {
                //GameObject.Find("LoosePanel").SetActive(true);
                Time.timeScale = 0;
            }

            string newHealth = healthF.ToString();
            GameObject.Find("HealthText").GetComponent<Text>().text = newHealth;

            //instanciar explosion en hq
            Instantiate(explosion, (headQuarters), Quaternion.identity);
                
            Destroy(gameObject, 1);


        }

    }
}
