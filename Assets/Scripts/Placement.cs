﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Placement : MonoBehaviour {

    [SerializeField] private GameObject machineGunPrefab, holdingWeapon;
    string energyText;


    // Use this for initialization
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
            energyText = GameObject.Find("MoneyText").GetComponent<Text>().text;

            if (holdingWeapon != null)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo))
                {
                    holdingWeapon.transform.position = hitInfo.point;
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (hitInfo.transform.CompareTag("Placement"))
                        {

                            holdingWeapon = null;
                            Destroy(hitInfo.collider);

                            float energyFloat = float.Parse(energyText);
                            energyFloat -= 4;
                            string newEnergy = energyFloat.ToString();
                            GameObject.Find("MoneyText").GetComponent<Text>().text = newEnergy;

                        }
                    }
                }
            }
    }

    public void InstantiateWeapon(string weapon)
    {
        if (weapon == "machineGun" && float.Parse(GameObject.Find("MoneyText").GetComponent<Text>().text) >= 4)
        {
            holdingWeapon = Instantiate(machineGunPrefab, Input.mousePosition, Quaternion.identity);
        }

    }
}
