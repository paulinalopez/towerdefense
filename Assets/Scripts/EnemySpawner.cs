﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemySpawner : MonoBehaviour {

    public enum EnemyTypes
    {
        buggy,
        helicopter, 
        hover
    };

    [Serializable]
    public struct Waves
    {
        public EnemyTypes[] wave;
    }

    public Waves[] waves;
    [SerializeField] private GameObject buggyPrefab;
    [SerializeField] private GameObject helicopterPrefab;
    [SerializeField] private GameObject hoverPrefab;
    [SerializeField] private int currentWave = 0;
    [SerializeField] private int currentEnemy = 0;
    [SerializeField] private int timeBetweenWaves = 5;

    public IEnumerator SendWave()
    {
        while (currentWave < waves.Length)
        {
            while (currentEnemy < waves[currentWave].wave.Length)
            {
                switch (waves[currentWave].wave[currentEnemy])
                {
                    case EnemyTypes.buggy:
                        Instantiate(buggyPrefab, this.transform.position, Quaternion.identity);
                        break;
                }

                yield return new WaitForSeconds(3);
                currentEnemy++;

            }
            currentWave++;

            currentEnemy = 0;
            yield return new WaitForSeconds(timeBetweenWaves);
        }

    }

    public void Wave()
    {
        StartCoroutine(SendWave());
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
