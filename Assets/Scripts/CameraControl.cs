﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    [SerializeField] private float speed = 300f;
    [SerializeField] private float border = 10f;
    [SerializeField] private Vector2 limit;

    [SerializeField] private float scrollSpeed = 20f;
    [SerializeField] private float minY = 20f;
    [SerializeField] private float maxY = 120f;

	
	// Update is called once per frame
	void Update ()
    {

        Vector3 pos = transform.position;

        //up
        if(Input.mousePosition.y >= Screen.height - border)
        {
            if(pos.z <= 200)
            {
            pos.z += speed * Time.deltaTime;
            }

        }

        //down
        if(Input.mousePosition.y <= border)
        {
            if (pos.z >= 50)
            {
            pos.z -= speed * Time.deltaTime;
            }

        }

        //right
        if (Input.mousePosition.x >= Screen.width - border)
        {
            if (pos.x <= 50)
            {
            pos.x += speed * Time.deltaTime;
            }

        }
        
        //left
        if (Input.mousePosition.x <= border)
        {
            if (pos.x >= -110)
            {
            pos.x -= speed * Time.deltaTime;
            }

        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        pos.y += scroll * scrollSpeed * 200 * Time.deltaTime;

        transform.position = pos;
    }

}
